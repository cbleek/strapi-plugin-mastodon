/**
 *
 * PluginIcon
 *
 */

import React from 'react';
import { Link } from '@strapi/icons';

const PluginIcon = () => <Link />;

export default PluginIcon;
