/*
 *
 * HomePage
 *
 */

import React, { useState, useEffect } from 'react';
import { Box, Flex, Tab, Typography } from '@strapi/design-system';
import pluginId from '../../pluginId';
import {LoadingIndicatorPage, useNotification} from '@strapi/helper-plugin';


const HomePage = () => {
  const [ setSetupInfo] = useState(null);
  const [isInProgress, setIsInProgress] = useState(false);
  const toggleNotification = useNotification();

  return <Flex alignItems="stretch" gap={4}>
    <Box padding={8} background="neutral100" width="100%">
      <Box paddingBottom={4}>
        <Typography variant="alpha">Mastodon Setup Information</Typography>
      </Box>
      <Box paddingTop={4} paddingBottom={4}>
        <Typography variant="pi" fontWeight="bold" textColor="neutral600" >ACTIONS</Typography>
      </Box>
    </Box>
  </Flex>
};

export default HomePage;
